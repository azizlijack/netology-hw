const apiRouter = require('./routes/api/index');
const rootRouter = require('./routes/root.router');
const createRouter = require('./routes/createBook.router');
require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');

const app = express();
const cors = require('cors')
const PORT = process.env.PORT || 3000;

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('view engine', 'ejs');

app.use("/api", apiRouter);
app.use("/create", createRouter);
app.use("/", rootRouter);

async function connectDB() {
  try {
    await mongoose.connect(`mongodb+srv://john:${process.env.DB_PASS}@bookscluster.klegz.mongodb.net/booksDB?retryWrites=true&w=majority`, {
      useNewUrlParser:true,
    });
    app.listen(PORT, () => {
      console.log(`=== start server PORT ${PORT} ===`);
    });
  } catch (error) {
    console.log("connectDB", error);
  }
}

connectDB();
