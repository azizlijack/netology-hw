const express = require('express');
const router = express.Router();

const booksRouter= require('./books.router');
const usersRouter = require('./users.router');

router.use("/user", usersRouter);
router.use("/books", booksRouter);

module.exports = router