const express = require('express');
const router = express.Router();
const booksController = require('../../controllers/books.controller');
const multer = require('multer')
const path = require('path')
const fs = require('fs');

const filesPath = path.resolve('uploads/');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!fs.existsSync(filesPath)) {
      fs.mkdirSync(filesPath, { recursive: true });
    }
    cb(null, filesPath)
  },
  filename: function (req, file, cb) {
    cb(null, req.body.id + path.extname(file.originalname))
  },
})

const upload = multer({ storage })

router.get('/', booksController.getBooks)
router.get('/:id/download', booksController.getFile)
router.get('/:id', booksController.getCurrentBook)
router.post('/', upload.single("file"), booksController.createBook)
router.put('/:id', upload.single("file"), booksController.updateBook)
router.delete('/:id', booksController.deleteBook);

module.exports = router;

