const express = require('express');
const router = express.Router();
const BooksController = require('../controllers/books.controller');

router.get("/", BooksController.getBooks)
router.get('/favicon.ico', (req,res)=> 'your faveicon');
router.get('/:id', BooksController.getCurrentBook)
router.get('/view/:id', (req, res) => BooksController.getCurrentBook(req, res, "view"))

module.exports = router;

