const books = require('../services/books.service');
const Books = require('../models/Book');
const express = require('express');
const path = require('path');
const app = express();
var cors = require('cors')

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

class BooksController {
  async getBooks(req, res) {
    const books = await Books.find({});
    res.render('index.ejs', { books });
  }

  async getCurrentBook(req, res, path = "update") {
    try {
      const { id } = req.params
      if (!id) {
        res.status(400).send({ status: 400, message: "Укажите id книги" });
        return
      }
      const currentBook = await Books.findById(id);
      if (!currentBook) {
        res.status(400).send({ status: 400, message: "id с такой книгой не сущесвует" });
        return
      }
      res.render('update.ejs', { book: currentBook, path });
    } catch (error) {
      console.log("getCurrentBook", error);
    }
  }

  getFile(req, res) {
    const { id } = req.params
    if (!id) {
      res.status(400).send({ status: 400, message: "Укажите id книги" });
      return
    }
    const hasId = books.getBooks().find(b => String(b.id) === String(id))
    if (!hasId) {
      res.status(400).send({ status: 400, message: "id с такой книгой не сущесвует" });
      return
    }

    if (!hasId.extFile) return
    res.download(path.resolve(`./uploads/${id}.${hasId.extFile}`));
  }

  async createBook(req, res) {
    let isErr = false

    if (isErr) {
      res.status(400).send({ status: 400, message: "Некорректные данные" });
      return
    }
    const books = await Books.find({});
    const hasId = books.find(b => String(b.id) === String(req.body.id))
    if (hasId) {
      res.status(400).send({ status: 400, message: "Книга с таким id уже существует" });
      return
    }
    let body = req.body
    if (req?.file) {
      const { originalname } = req?.file;
      const lastEl = originalname?.split(".").length - 1
      const extFile = originalname.split(".")[lastEl]
      body = { ...body, extFile };
    }

    const newBooks = await Books(req.body);
    await newBooks.save();
    res.send({ status: 200, message: "Книга создана" });
  }

  async updateBook(req, res) {
    try {
      const { id } = req.params
      if (!id) {
        res.status(400).send({ status: 400, message: "Укажите id книги" });
        return
      }

      await Books.findOneAndUpdate({ _id: id }, req.body);
    } catch (error) {
      console.log("updateBook", error);
    }

  }

  async deleteBook(req, res) {
    try {
      const { id } = req.params
      if (!id) {
        res.status(400).send({ status: 400, message: "Укажите id книги" });
        return
      }
      await Books.deleteOne({ _id: id })
      res.send({ status: 200, message: "Книга удалена" });
    } catch (error) {
      console.log("deleteBook", error);
    }

  }
}

const booksController = new BooksController()

module.exports = booksController