// Задание 1
const readline = require('readline');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('../1.2/logColors');
const fs = require('fs');
const path = require('path');

const argv = yargs(hideBin(process.argv)).argv;
const {fileName, f} = argv;

if(!fileName && !f) {
  console.log(logColors.red, 'Укажите имя файла в консоле. Например, -f result или fileName result');
  return;
}
const filePath = path.join(__dirname, "files", `${fileName || f}.txt`);
 
fs.readFile(filePath, 'utf8', function(err, contents) {
  try {
    if(!contents) throw new Error();
    const fileData = contents.split(",");
    console.log("fileData", fileData);
    const tryes = fileData.filter((v)=> v === "start");
    const errTryes = fileData.filter((v)=> v === "err");
    const succsess = fileData.filter((v)=> v === "true");
    const allTryes = fileData.filter((v)=> v !== "start" && v !== "");
    const succsessPerc = (succsess.length / allTryes.length) *100;
    const errPerc = (errTryes.length / allTryes.length) *100;
    console.log("общее количество партий", tryes.length);
    console.log("ошибок", errTryes.length);
    console.log("удачных попыток", succsess.length);
    console.log("allTryes", allTryes.length);
    console.log("удачных в %", succsessPerc);
    console.log("не удачных в %", errPerc);
  } catch (error) {
    console.log("файла с таким именем не существует");
  }
});


