// Задание 1
const readline = require('readline');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('../1.2/logColors');
const fs = require('fs');
const path = require('path');

const argv = yargs(hideBin(process.argv)).argv;
const {fileName, f} = argv;

if(!fileName && !f) {
  console.log(logColors.red, 'Укажите имя файла в консоле. Например, -f result или fileName result');
  return;
}

const filePath = path.join(__dirname, "files", `${fileName || f}.txt`);
fs.readFile(filePath, "utf8", function(error, data){
    if(error) {
      fs.writeFile(filePath, "start,", function(err) {
        if(err) {
          return  console.log(logColors.red, err);
        }
      }); 
      return
    }
    appendInFile("start,")
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// 1 or 2
const randomNumb = randomInteger(1, 2);
let isGuessed = false;
rl.write('Загадано число 1 или 2, попробуйте отгадать  \n');

rl.on('line', (data) => {
  if(!Number(data)) {
    console.log(logColors.red, 'Неверный формат, укажите число');
    return
  }
  if(Number(data) > Number(randomNumb)) {
    console.log(logColors.light, "Не верно");
    appendInFile("err,")
    return;
  }
  if(Number(data) < Number(randomNumb)) {
    console.log(logColors.light, "Не верно");
    appendInFile("err,")
    return;
  }
  if(Number(data) === Number(randomNumb) || data === "exit") {
    isGuessed = true;
    appendInFile("true,")
    rl.close();
  }
}); 

rl.on('close', () => {
  if(!isGuessed) {
    console.log(logColors.cyan, "Число не отгадано")
    return
  }
  console.log(logColors.green, `Отгадано число ${randomNumb}`)
});


function appendInFile(val) {
  fs.appendFile(filePath, val, function(error){
    if(error) {
      console.log('error', error);
      return;
    }
});
}

function randomInteger(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}