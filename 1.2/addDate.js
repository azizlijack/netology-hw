#!/usr/bin/env node
// Задание 1 cmd add -d 2
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('./logColors');

const argv = yargs(hideBin(process.argv)).argv;
const today = new Date();
const { date, d  } = argv;
const oneDay = 1000 * 60 * 60 * 24;


try {
	if(date || d) {
		const culcDay = date || d;
		const culcDate =  (oneDay * culcDay) + today.getTime();
		console.log(logColors.green, `culc day is: ${new Date(culcDate).toISOString()}`);  
		return;
	}
	console.log(logColors.red, 'укажите флаг -d или --date со значением');  
} catch (error) {
	console.log(logColors.red, `descriptrion error: ${error}`);
}

