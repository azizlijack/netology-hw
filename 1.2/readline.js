// Задание 2
const readline = require('readline');
const { logColors } = require('./logColors');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const randomNumb = Math.floor(
  Math.random() * (100 - 0) + 0
)
let isGuessed = false;

rl.write('Загадано число в диапазоне от 0 до 100  \n');

rl.on('line', (data) => {
  if(!Number(data)) {
    console.log(logColors.red, 'Неверный формат, укажите число');
    return
  }
  if(Number(data) > Number(randomNumb)) {
    console.log(logColors.light, "Меньше");
    return;
  }
  if(Number(data) < Number(randomNumb)) {
    console.log(logColors.light, "Больше");
    return;
  }
  if(Number(data) === Number(randomNumb) || data === "exit") {
    isGuessed = true;
    rl.close();
  }
}); 

rl.on('close', () => {
  if(!isGuessed) {
    console.log(logColors.cyan, "Число не отгадано")
    return
  }
  console.log(logColors.green, `Отгадано число ${randomNumb}`)
});
