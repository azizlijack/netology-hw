const colors = { red:'\x1b[31m%s\x1b[0m', green:'\x1b[32m%s\x1b[0m', light:"\x1b[1m", cyan: "\x1b[36m" };

module.exports = { logColors: colors };