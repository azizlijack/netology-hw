#!/usr/bin/env node
// Задание 1
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('./logColors');

const argv = yargs(hideBin(process.argv)).argv;
const today = new Date();
const month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
const {y, year, month:isMonth, m, date, d  } = argv;

try {
	if(year || y) {
		console.log(logColors.cyan, `Year is: ${today.getFullYear()}`);  
		return;
	}
	
	if(isMonth || m) {
		console.log(logColors.green, `month is: ${month[today.getMonth()]}`);  
		return;
	}
	if(date || d) {
		console.log(logColors.green, `day is: ${today.getDate()}`);  
		return;
	}
	console.log(logColors.green, `today is: ${ today.toISOString()}`);  
} catch (error) {
	console.log(logColors.red, `descriptrion error: ${error}`);
}

