#!/usr/bin/env node
// Задание 1 sub --month 1
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('./logColors');

const argv = yargs(hideBin(process.argv)).argv;
const today = new Date();
const { month, m  } = argv;
const oneDay = 1000 * 60 * 60 * 24;
const oneMonth = oneDay * 31;

try {
	if(month || m) {
		const culcMonth = month || m;
		const culcDate = today.getTime() - (oneMonth * culcMonth);
		console.log(logColors.green, `result day is: ${new Date(culcDate).toISOString()}`);  
		return;
	}
	console.log(logColors.red, 'укажите флаг -m или --month со значением');  
} catch (error) {
	console.log(logColors.red, `descriptrion error: ${error}`);
}


