require('dotenv').config();
const http = require('http');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { logColors } = require('../1.2/logColors');
const {apiKey} = process.env;

const argv = yargs(hideBin(process.argv)).argv;
const {city, c} = argv;

if(!city && !c) {
  console.log(logColors.red, 'Укажите город в консоле. Например, -f samara или city samara');
  return;
}
const cityName = city || c;
const preparedData = []
const url = `http://api.weatherstack.com/current?access_key=${apiKey}&query=${cityName}`;
const req = http.request(url, res => {
  res.on('data', data => preparedData.push(data.toString()))
  res.on('end', () => {
    try {
      const {current: {temperature}} = JSON.parse(preparedData);
      console.log(`в ${cityName} сейчас`, temperature);
    } catch (error) {
      console.log(logColors.red, 'Возникла ошибка, укажите другой город или обратитесь в поддержку');
    }
  })
  res.on('error', () => {
    console.log(logColors.red, 'Возникла ошибка, укажите другой город или обратитесь в поддержку');
  });
})

req.end();