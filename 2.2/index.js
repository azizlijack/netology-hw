const { booksRouter } = require('./routes/books.router');
const {usersRouter } = require('./routes/users.router');
const express = require('express');
const app = express();
const cors = require('cors')
const port = 3000;

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/api/user", usersRouter);
app.use("/api/books", booksRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})