const books = require('../services/books.service');
const express = require('express');
const app = express();
var cors = require('cors')

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


const keys = [
  "id",
  "title",
  "description",
  "authors",
  "favorite",
  "fileCover",
  "fileName",
]


class BooksController {
  getBooks(req, res) {
    return res.status(200).send(books.getBooks())
  }

  getCurrentBook(req, res) {
    const { id } = req.params
    if (!id) {
      res.status(400).send("Укажите id книги");
      return
    }
    const hasId = books.getBooks().find(b => String(b.id) === String(id))
    console.log("hasId", books.getBooks(), id);
    if (!hasId) {
      res.status(400).send("id с такой книгой не сущесвует");
      return
    }

    res.send(books.getCurrentBook(id))
  }

  createBook(req, res) {
    let isErr = false
    Object.keys(req.body).forEach(d => {
      if (isErr) return
      const hasOtherKey = keys.includes(d)
      if (!hasOtherKey) isErr = true
    })
    if (isErr) {
      res.status(400).send("Некорректные данные");
      return
    }
    const hasId = books.getBooks().find(b => String(b.id) === String(req.body.id))
    if (hasId) {
      res.status(400).send("Книга с таким id уже существует");
      return
    }
    books.createBook(req.body)
    res.send("Книга создана");
  }

  updateBook(req, res) {
    const { id } = req.params
    if (!id) {
      res.status(400).send("Укажите id книги");
      return
    }
    const hasId = books.getBooks().find(b => String(b.id) === String(id))

    if (!hasId) {
      res.status(400).send("id с такой книгой не сущесвует");
      return
    }

    res.send(books.updateBook(id, req.body))
  }

  deleteBook(req, res) {
    const { id } = req.params
    if (!id) {
      res.status(400).send("Укажите id книги");
      return
    }
    const hasId = books.getBooks().find(b => String(b.id) === String(id))
    if (!hasId) {
      res.status(400).send("id с такой книгой не сущесвует");
      return
    }

    books.deleteBook(id)
    res.send("Книга удалена");
  }
}

const booksController = new BooksController()

module.exports = booksController