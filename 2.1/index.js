const booksController = require('./controllers/books.controller');
const usersController = require('./controllers/users.controller');
const express = require('express');
const app = express();
var cors = require('cors')
const port = 3000;

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.post('/api/user/login', (req, res) => {
  usersController.createUser(req, res)
})

app.get('/api/books/', (req, res) => {
  booksController.getBooks(req, res)
});

app.get('/api/books/:id', (req, res) => {
  booksController.getCurrentBook(req, res)
})

app.post('/api/books', (req, res) => {
  booksController.createBook(req, res)
})

app.put('/api/books/:id', (req, res) => {
  booksController.updateBook(req, res)
})

app.delete('/api/books/:id', (req, res) => {
  booksController.deleteBook(req, res)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})