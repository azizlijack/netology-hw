use mongo;
db.createColleaction("books");
** добавление в коллекцию 
db.books.insertMany([
  {
    title:"book -1",
    description:"description book -1",
    authors:"authors book -1"
  },
  {
    title:"book -2",
    description:"description book -2",
    authors:"authors book -2"
  },
  {
    title:"book -3",
    description:"description book -3",
    authors:"authors book -3"
  },
])

** поиск
db.books.find({title:"book -1"})

** обновление
db.books.updateOne(
  {_id: ObjectId("6210d97e864c023cfd4e9318")}, // предполагается, что мы знаешь конкрентный id книги
  {$set: {
    title: "new book", description: "new description book -1"
    }
  }
)